 /*
 * MAIN Generated Driver File
 * 
 * @file main.c
 * 
 * @defgroup main MAIN
 * 
 * @brief This is the generated driver implementation file for the MAIN driver.
 *
 * @version MAIN Driver Version 1.0.0
*/

/*
� [2022] Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip 
    software and any derivatives exclusively with Microchip products. 
    You are responsible for complying with 3rd party license terms  
    applicable to your use of 3rd party software (including open source  
    software) that may accompany Microchip software. SOFTWARE IS ?AS IS.? 
    NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS 
    SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,  
    MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT 
    WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY 
    KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF 
    MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE 
    FORESEEABLE. TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP?S 
    TOTAL LIABILITY ON ALL CLAIMS RELATED TO THE SOFTWARE WILL NOT 
    EXCEED AMOUNT OF FEES, IF ANY, YOU PAID DIRECTLY TO MICROCHIP FOR 
    THIS SOFTWARE.
*/
#include "mcc_generated_files/system/system.h"
#include "lcd.h"

unsigned int a;
uint8_t b;

void minhaFucao(void){
    NOP();
    a++;
}

/*
    Main application
*/

int main(void)
{
    uint16_t master;
    SYSTEM_Initialize();

    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts 
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts 
    // Use the following macros to: 
    
    Timer2_OverflowCallbackRegister(minhaFucao);

    // Enable the Global Interrupts 
    INTERRUPT_GlobalInterruptEnable(); 

    // Disable the Global Interrupts 
    //INTERRUPT_GlobalInterruptDisable(); 

    // Enable the Peripheral Interrupts 
    INTERRUPT_PeripheralInterruptEnable(); 

    // Disable the Peripheral Interrupts 
    //INTERRUPT_PeripheralInterruptDisable(); 

    master = ADC_GetConversion(channel_ANA0);
    
        
    __delay_ms(250);CLRWDT();
    __delay_ms(250);CLRWDT();
    __delay_ms(250);CLRWDT();
    __delay_ms(250);CLRWDT();
    
    __delay_ms(250);CLRWDT();
    __delay_ms(250);CLRWDT();
    __delay_ms(250);CLRWDT();
    __delay_ms(250);CLRWDT();
    
    a=0;
    b=0;
    lcd_init();
    __delay_ms(250);CLRWDT();
    lcd_init();

    while(1)
    {
        lcd_clear();
        lcd_goto(b);
        lcd_puts("UM PLANETA MELHOR");
        lcd_goto2(0);
        lcd_puts("CAIQUE");
        lcd_putch(0x20);
        lcd_num(a);
        if(++b>15)b=0;
    
    __delay_ms(450);CLRWDT();
    
    
    }    
}