/*
 *	LCD interface.
 *	It uses it in 4 bit mode.
 *  Controlador ST7066U
 *	Display AGM 1602E-808
 *	PORTC bits 2-5 are connected to the LCD data bits 4-7 (high nibble)
 *	PORTC bit 0 is connected to the LCD EN input (enable)
 *	PORTC bit 1 is connected to the LCD RS bit (register select)
 *	
 *	
 */



#include "mcc_generated_files/system/system.h"
#include	"lcd.h"


 
#define HIGH		1
#define LOW		0 

//definicao dos bits de controle utilizado pelo LCD
#define LCD_RS		LATCbits.LATC1
#define LCD_EN		LATCbits.LATC0	

//definindo a porta de dados utilizada pelo LCD
#define LCD_PORTA	LATC

//definindo o nible utilizado pelo LCD
#define LCD_NIBLE	HIGH


#define	LCD_STROBE	((LCD_EN = 1),(LCD_EN = 0))

void lcd_write(unsigned char c)//write a byte to the LCD in 4 bit mode */
{
#if LCD_NIBLE == HIGH
	LCD_PORTA = (LCD_PORTA & 0x03) |  ((c & 0xf0)>>2);
	LCD_STROBE;
	LCD_PORTA = (LCD_PORTA & 0x03) |  ((c & 0x0F)<<2);
	LCD_STROBE;

#elif LCD_NIBLE == LOW
	LCD_PORTA = c>>4;
	LCD_STROBE;
	LCD_PORTA = c;
	LCD_STROBE;
#endif
	//waitus(_T50);
	__delay_us(50);
	
}

void lcd_write_init(unsigned char c)//write a byte to the LCD in 4 bit mode */
{
#if LCD_NIBLE == HIGH
	
	LCD_PORTA = (LCD_PORTA & 0x03) |  ((c & 0x0F)<<2);
	

#elif LCD_NIBLE == LOW
	//LCD_PORTA = c>>4; FAZER AJUSTE CONFORME NECESSARIO
	//LCD_STROBE;
	//LCD_PORTA = c;
	//LCD_STROBE;
    NOP();
#endif
	//waitus(_T50);
	__delay_us(50);
	
}

void lcd_clear(void) //Clear and home the LCD
{
	LCD_RS = 0;
	lcd_write(0x1);
	//waitMs(2);
	__delay_ms(2);
	
}

void lcd_puts(const char * s)/* write a string of chars to the LCD */ 
{
	LCD_RS = 1;	// write characters
	while(*s)
		lcd_write(*s++);
}

void
lcd_putch(char c)// write one character to the LCD */
{
	LCD_RS = 1;	// write characters
	lcd_write(c);
}

void lcd_num(unsigned int num){//write #, 3 digits

	 unsigned char none[5],i;

	i = 6;
	while(--i){
		none[i-1] =(unsigned char)(num%10 + 0x30);
		num = num/10;
	}
//	lcd_putch(none[0]);
	lcd_putch(none[1]);
	lcd_putch(none[2]);
	lcd_putch(none[3]);
	lcd_putch(none[4]);
	//lcd_putch(0x20);

}
void lcd_num2(unsigned int num){//write #, 3 digits

	 unsigned char none[5],i;

	i = 6;
	while(--i){
		none[i-1] =(unsigned char)(num%10 + 0x30);
		num = num/10;
	}
//	lcd_putch(none[0]);
	lcd_putch(none[1]);
	lcd_putch(none[2]);
	lcd_putch(58);
	lcd_putch(none[3]);
	lcd_putch(none[4]);


}


void lcd_goto(unsigned char pos)// Go to the specified position
{
	LCD_RS = 0;
	lcd_write(0x80+pos);
}

void lcd_goto2(char x ){//inicializa na segunda linha e com desloca p a esquerda*/	
	LCD_RS = 0;
	lcd_write(0xc0);
	if(!x)x=1;else x++;
	while(--x)
		lcd_write(0x14);
}

void LCD_Print(char *temp){
    
    lcd_putch(temp[0]);
    lcd_putch(temp[1]);
    lcd_putch(temp[2]);
    lcd_putch(0x2e);
    lcd_putch(temp[4]);
    lcd_putch(temp[5]);
    lcd_putch(temp[6]);
    lcd_putch(temp[7]);
}



void lcd_init(void)//initialise the LCD - put into 4 bit mode */
{
    __delay_ms(150);
	LCD_RS = 0;	// write control bytes
    LCD_EN = 0;
	__delay_us(50);
	lcd_write_init(0x03);	// set 4 bit mode
	LCD_STROBE;
	__delay_us(50);
    
	lcd_write_init(0x02);
	LCD_STROBE;
    lcd_write_init(0x00);
	LCD_STROBE;
	__delay_us(50);
    
	lcd_write_init(0x02);
	LCD_STROBE;
    lcd_write_init(0x00);
	LCD_STROBE;
	__delay_us(50);
    
    lcd_write_init(0x00); //control 1 line or 2 lines   display on/off
    LCD_STROBE;
    lcd_write_init(0x0f); //control 1 line or 2 lines   display on/off
    LCD_STROBE;
	__delay_us(80);

    //inicializacao de controle do display
    //seta o blink aqui
    lcd_write_init(0x00); //lcd clear	
    LCD_STROBE;
    lcd_write_init(0x01); //lcd clear	
    LCD_STROBE;
    __delay_ms(10);
    
    
    lcd_write_init(0x00); //lcd clear	
    LCD_STROBE;
    lcd_write_init(0x06); //lcd clear	
    LCD_STROBE;
    __delay_ms(10);
	//lcd_write(0x0e); // display on/off control blink/cursor	
	//lcd_write(0x01);//lcd clear	

    //entry set mode
	//lcd_write(0x06);	// entry mode decrement/increment mode shitf on/off 
    	__delay_ms(80);
}
