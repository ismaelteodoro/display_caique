/* 
 * File:   lcd.h
 * Author: ismael.souza
 *
 * Created on March 16, 2021, 5:39 PM
 */

#ifndef LCD_H
#define	LCD_H

#ifdef	__cplusplus
extern "C" {
#endif
    
//#define _XTAL_FREQ 32000000    

 void lcd_write(unsigned char);

/* Clear and home the LCD */

 void lcd_clear(void);

/* write a string of characters to the LCD */

 void lcd_puts(const char * s);

/* Go to the specified position */

 void lcd_goto(unsigned char pos);
	
/* intialize the LCD - call before anything else */

 void lcd_init(void);

 void lcd_putch(char);

 void lcd_num(unsigned int);
void lcd_num2(unsigned int);
void lcd_num3(unsigned long);

 void lcd_goto2(char);
 
 void LCD_Print(char *temp);

/*	Set the cursor position */

#define	lcd_cursor(x)	lcd_write(((x)&0x7F)|0x80)

//extern void DelayMs(unsigned char);

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_H */

